const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const studentSchema = new Schema({
    email: {
        type: String
        // unique:true
    },
    name : String,
    dob : Date,
    gender: {
        type: String,
        // enum: ['Male', 'Female', 'Other']
    },
    contact: String,
    address : String,
    city : String,
    state : String,
    pin : String,
    course : String,
    college : String,
    room : String,
    floor : String,
    tower : String,
},
{ timestamps: true });

module.exports = mongoose.model('Student', studentSchema);