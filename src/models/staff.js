const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const staffSchema = new Schema({
    email: {
        type: String
        // unique:true
    },
    name : String,
    dob : Date,
    gender:  String,
    contactNumber: String,
    address : String,
    city : String,
    state : String,
    pin : String,
    position : String
},
{ timestamps: true });

module.exports = mongoose.model('Staff', staffSchema);