const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const roomSchema = new Schema({
    room : String,
    floor : String,
    tower: String,
    student : {
        type: mongoose.Types.ObjectId,
        ref:  'student' 
    },
},
{ timestamps: true });

module.exports = mongoose.model('room', roomSchema);