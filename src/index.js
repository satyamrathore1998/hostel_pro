const express = require('express');

const bodyparser = require('body-parser');


// import mongoose
const mongoose = require('mongoose');
const cors = require('cors');

var app = require('express')();
app.use(bodyparser.json())
var http = require('http').Server(app);

// import path
 const path = require('path');
 app.use(cors());


const environments = require('./environment/environment');


require('./routes/student')(app);   
require('./routes/staff')(app);
require('./routes/room')(app);
// require('./routes/user')(app);
 


// header middleware

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, OPTIONS, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers',
        'X-Requested-With,Content-type, Authorization, x-access-token');

    if (req.method === 'OPTIONS') {
        res.status(200).end();
    } else {
        next();
    }
});

// cors middleware
//app.options('*', cors());


app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'public') + '/index.html')
})

app.use((req, res, next) => {
    const err = new Error();
    err.message = 'Not Found';
    err.status = 404;
    next(err);
});



mongoose.connect(
    environments.MONGO_URL + '/' + environments.DB_NAME, {
        useNewUrlParser: true
    })
.then((db) => {
    console.log(`Worker  Connected to DB ${environments.DB_NAME} Successfully!`);
})
.catch((e) => {
    console.log("error occured in db connection: ", e);
})





http.listen(5000, () => {
    console.log(`Server Started. Listening on 5000 !`);
});