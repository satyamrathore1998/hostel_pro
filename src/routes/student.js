const studentController= require('../controllers/studentcontroller');

module.exports = (app) => {
    const student = '/api/student/';
    app.post(`${student}add`,studentController.addStudent);
    app.post(`${student}update`,studentController.updateStudent);
    app.post(`${student}get`,studentController.getStudent);
    app.post(`${student}delete`,studentController.deleteStudent);
}