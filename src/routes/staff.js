const staffController = require('../controllers/staffcontroller');

module.exports = (app) => {
    const staff = '/api/staff/';
    app.post(`${staff}add`,staffController.addStaff);
    app.post(`${staff}update`,staffController.updateStaff);
    app.post(`${staff}get`,staffController.getStaff);
    app.post(`${staff}delete`,staffController.deleteStaff);
}