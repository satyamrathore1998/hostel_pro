const roomController = require('../controllers/roomcontroller');

module.exports = (app) => {
    const room = '/api/room/';

    app.post(`${room}getAll`,roomController.getRoomDetails);
}