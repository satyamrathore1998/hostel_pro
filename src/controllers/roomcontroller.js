const async =  require('async');
const Room =  require('../models/room');
const sendResponse = require('../shared/sendResponse');

async function getRoomDetails(req,res){
    let rooms = await Room.find();
    if(!!rooms){
      sendResponse(res,200,"Found Room details!",rooms);
    }else{
        sendResponse(res,201,"Not Found Rooms!");
    }
}


module.exports = {
    getRoomDetails
}