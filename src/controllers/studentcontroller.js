const async =  require('async');
const Student =  require('../models/student');
const sendResponse = require('../shared/sendResponse');

async function getStudent(req,res){
    let student = await Student.find();
    if(student){
      sendResponse(res,200,"Found student details!",student);
    }else{
        sendResponse(res,201,"Not Found student!");
    }
}

async function addStudent(req,res){
   console.log("Body====",req.body);
    let student = await Student.create(req.body);

    if(student){
      sendResponse(res,200,"Added student details!",student);
    }else{
        sendResponse(res,201,"Not Added student!");
    }
}

async function updateStudent(req,res){
    
    let student = await Student.findOneAndUpdate({_id : req.body.id }, {$set : req.body});
    if(student){
      sendResponse(res,200,"Updated student details!",student);
    }else{
        sendResponse(res,201,"Not updated student!");
    }
}

async function deleteStudent(req,res){
    
    let student = await Student.remove({_id : req.body.id});
    if(student){
      sendResponse(res,200,"Deleted student details!");
    }else{
        sendResponse(res,201,"Not deleted student!");
    }
}




module.exports = {
    addStudent,
    getStudent,
    updateStudent,
    deleteStudent

}