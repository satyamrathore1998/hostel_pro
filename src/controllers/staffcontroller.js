const async =  require('async');
const Staff =  require('../models/staff');
const sendResponse = require('../shared/sendResponse');

async function getStaff(req,res){
    let staffs = await Staff.find();
    if(staffs){
      sendResponse(res,200,"Found Staff details!",staffs);
    }else{
        sendResponse(res,201,"Not Found Staff!");
    }
}

async function addStaff(req,res){

    let staffs = await Staff.create(req.body);
    if(staffs){
      sendResponse(res,200,"Added Staff details!",staffs);
    }else{
        sendResponse(res,201,"Not Added Staff!");
    }
}

async function updateStaff(req,res){
    
    let staffs = await Staff.findOneAndUpdate({_id : req.body.id }, {$set : req.body});
    if(staffs){
      sendResponse(res,200,"Updated Staff details!",staffs);
    }else{
        sendResponse(res,201,"Not Added Staff!");
    }
}

async function deleteStaff(req,res){
    
    let staffs = await Staff.remove({_id : req.body.id});
    if(staffs){
      sendResponse(res,200,"Deleted Staff details!");
    }else{
        sendResponse(res,201,"Not Added Staff!");
    }
}




module.exports = {
    addStaff,
    getStaff,
    updateStaff,
    deleteStaff

}